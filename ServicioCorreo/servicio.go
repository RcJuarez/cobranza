package servicio

import (
	"fmt"
	"time"

	"../Variables"
	"./Correo"
	"./Elastic"
	"github.com/jasonlvhit/gocron"
)

//Daemon Demonio que lanza la tarea y la programa
func Daemon() {
	s := gocron.NewScheduler()
	//<<<<<<< HEAD
	//	s.Every(60).Minutes().Do(Task)
	//=======
	s.Every(90).Minutes().Do(Task)
	//>>>>>>> fc9dcdc2c29691b121d3fb137ba6a2b6072ba821
	<-s.Start()
}

//Task ejecuta tarea programada
func Task() {
	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	t := time.Now().In(time.Local)
	fmt.Println("Lanza Busqueda", t)
	if t.Hour() < 20 && t.Hour() > 8 && t.Weekday() != time.Sunday {
		cliente := elasticconnector.ConectarElasticSearch(DataCfg.InfElastic.Servidor, DataCfg.InfElastic.Puerto)
		resultado := elasticconnector.GetAllElasticSearch(DataCfg.InfElastic.NombreBase, cliente)
		// correo.IngresarTodos(resultado, cliente)
		correo.ExtractInformationElasticSearch(resultado, cliente)
		cliente.CloseIndex("dashfacturastyc")
		fmt.Println("Horario de oficina")
	} else {
		fmt.Println("Horario No laborable")
	}

}
