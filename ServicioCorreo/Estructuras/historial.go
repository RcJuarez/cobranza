package estructura

//ObjetoCorreo es una estructiura que rescata los registros de elastic
// y guarda sus datos para ser comparados y decidir el envio de correo electronico
type ObjetoCorreo struct {
	Folio       float64 `json:"foliofactura"`
	Serie       string  `json:"seriefactura"`
	Emision     string  `json:"fechafactura"`
	Vencimiento string  `json:"fechavencimientofactura"`
	Saldo       float64 `json:"saldofactura"`
	Codigo      string  `json:"codigocliente"`
	Nombre      string  `json:"nombrecliente"`
	Correo      string  `json:"correocliente,omitempty"`
	Telefono    string  `json:"telefonocliente,omitempty"`
	Direccion   string  `json:"direccioncliente,omitempty"`
	Dias        float64 `json:"dias"`
	Movimiento  string  `json:"nummov"`
}

/*
indice: saldofactura        tipo: float64
 indice: ADD        tipo: string
 indice: @timestamp        tipo: string
 indice: dias        tipo: float64
 indice: @version        tipo: string
 indice: codigocliente        tipo: string
 indice: foliofactura        tipo: float64
 indice: fechafactura        tipo: string
 indice: correocliente        tipo: string
 indice: telefonocliente        tipo: string
 indice: nombrecliente        tipo: string
 indice: direccioncliente        tipo: %!s(<nil>)
 indice: fechavencimientofactura        tipo: string
 indice: seriefactura        tipo: string
 indice: nummov        tipo: float64



*/
