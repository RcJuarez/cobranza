package elasticconnector

import (
	"context"
	"fmt"

	elastic "gopkg.in/olivere/elastic.v5"
)

//cargar la seeciion establecida en el archivo de configuracion
//var DataM, errLectArch = MoVar.CargaSeccionCFG(MoVar.SecElastic)

//Constantes Propias de la conexion
const (
	//IP                     = "http://10.0.1.243:9200" //IP Direccion del servidor ELASTICSEARCH
	//INDICEELASTICSEARCH    = "dashfacturastyc"       //INDICE Indice de ELASTICSEARCH
	COLECCIONELASTICSEARCH = "dashfacturastyc" //Coleccion de ELASTICSEARCH
	//IP                     = "http://localhost:9200" //IP Direccion del servidor ELASTICSEARCH
	// INDICEELASTICSEARCH      = "sgc"                   //INDICE Indice de ELASTICSEARCH
	// COLECCIONELASTICSEARCH   = "facturas"              //Coleccion de ELASTICSEARCH
	TOTALRESULTADOSDEVUELTOS = 10000 //Elementos devueltos
)

/*
 ConectarElasticSearch : Establece una conexion al servidor de elasticsearch
 Parametros:
		-ip: Direccion ip
 Retorno:
		-cliente:  Regresa un cliente de elasticSearch
 Autor: Ramon
 Fecha: 09/03/2017
 Revision: 1.0
*/
func ConectarElasticSearch(ip, puerto string) *elastic.Client {
	rutaElastic := "http://" + ip + ":" + puerto
	fmt.Println("Ruta elastic, ", rutaElastic)
	client, err := elastic.NewClient(elastic.SetURL(rutaElastic))
	if err != nil {
		fmt.Println("Error en elastic search", err)
	}
	return client
}

/*
 GetAllElasticSearch : Realiza una busqueda de todos los documentos existentes en elasticsearch
 Parametros:
		-texto: Cualquier palabra, texto o dato a buscar
		-client: un cliente ya establecido para la busqueda
 Retorno:
		-docs:  Una serie (coleccion) de resultados, cons las coincidencias del texto ingresado
 Autor: Ramon
 Fecha: 09/03/2017
 Revision: 1.0
*/
func GetAllElasticSearch(indice string, client *elastic.Client) *elastic.SearchResult {
	QueryTodos := elastic.NewMatchAllQuery()

	docs, err := client.Search().
		Index(indice).
		Type(COLECCIONELASTICSEARCH).
		Query(QueryTodos).
		// Sort("dias", true).
		From(0).
		Size(TOTALRESULTADOSDEVUELTOS).
		Do(context.TODO())
	if err != nil {
		fmt.Println("Error al realizar la busqueda")
		fmt.Println(err)
	}
	return docs
}
