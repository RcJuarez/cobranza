package mongoconn

import (
	"fmt"

	"../../Variables"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Scheduler estructura que se utiliza para almacenar la frecuencia de los avisos
type Scheduler struct {
	ID               bson.ObjectId `bson:"_id,omitempty"`
	CorreoFrecuencia int           `bson:"correofrecuencia"`
	Aviso            int           `bson:"aviso"`
	Domingo          bool          `bson:"domingo,omitempty"`
	Lunes            bool          `bson:"lunes,omitempty"`
	Martes           bool          `bson:"martes,omitempty"`
	Miercoles        bool          `bson:"miercoles,omitempty"`
	Jueves           bool          `bson:"jueves,omitempty"`
	Viernes          bool          `bson:"viernes,omitempty"`
	Sabado           bool          `bson:"sabado,omitempty"`
}

//ConectaMgo establece conexion a mongo
func ConectaMgo() *mgo.Session {
	DataCfg, err := MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	} else {
		session, err := mgo.Dial(DataCfg.InfMongo.Servidor)
		// session, err := mgo.Dial("localhost")
		if err != nil {
			fmt.Println(err)
		}
		return session
	}
	return nil
}

//SetConfiguracion a mongo
//Recibe data de tipo estructura
//Regresa insertado de tipo boleano
func SetConfiguracion(data Scheduler) bool {

	var insertado bool
	insertado = false

	DataCfg, err := MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	} else {
		s := ConectaMgo()
		d := s.DB(DataCfg.InfMongo.NombreBase)
		err := d.Login(DataCfg.InfMongo.Usuario, DataCfg.InfMongo.Pass)
		if err != nil {
			fmt.Println("No esta autorizado para conectarse a la base de datos.", err)
			insertado = false
			fmt.Println(err)
		} else {
			fmt.Println("Se ha autenticado al usuario:)")
			c := d.C("scheduler")
			err = c.Insert(data)
			if err != nil {
				fmt.Println("Er:(", err)
				insertado = false
			} else {
				fmt.Println("Insercion Correcta :)")
				insertado = true
			}
			d.Logout()
		}

		s.Close()
	}
	return insertado

}

//DelConfiguracion Funcion que elimina la configuracion del Scheduler
func DelConfiguracion() bool {
	var eliminado bool
	DataCfg, err := MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	} else {
		s := ConectaMgo()
		d := s.DB(DataCfg.InfMongo.NombreBase)
		err := d.Login(DataCfg.InfMongo.Usuario, DataCfg.InfMongo.Pass)
		if err != nil {
			fmt.Println("No esta autorizado para conectarse a la base de datos.", err)
			return false
		}
		c := d.C("scheduler")

		err = c.DropCollection()

		defer s.Close()

		if err != nil {
			fmt.Println("Er:(", err)
			eliminado = false
		} else {
			fmt.Println("OK:)")
			eliminado = true
		}
		d.Logout()
		defer s.Close()
	}

	return eliminado
}

//GetConfiguracion funcion para obtener los parametros del Scheduler
func GetConfiguracion() Scheduler {
	var result Scheduler

	DataCfg, err := MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	} else {
		s := ConectaMgo()
		d := s.DB(DataCfg.InfMongo.NombreBase)
		err := d.Login(DataCfg.InfMongo.Usuario, DataCfg.InfMongo.Pass)
		if err != nil {
			fmt.Println("No esta autorizado para conectarse a la base de datos.", err)
			fmt.Println(err)
		} else {
			fmt.Println("Se ha autenticado al usuario, se procede a la lectura =)")
			c := d.C("scheduler")
			c.Find(nil).One(&result)
		}
		d.Logout()
		s.Close()
	}

	return result
}
