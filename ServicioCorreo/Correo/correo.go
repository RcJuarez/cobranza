package correo

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/smtp"
	"time"

	"math"

	"../../Variables"
	"../Conexiones"
	"../Elastic"
	"gopkg.in/mgo.v2/bson"
	elastic "gopkg.in/olivere/elastic.v5"
)

//ObjetoCorreo objeto que se encarga de tratar los datos obtenidos de elasticsearch
type ObjetoCorreo struct {
	Folio            float64   `json:"foliofactura"`
	Serie            string    `json:"seriefactura"`
	Emision          string    `json:"fechafactura"`
	Vencimiento      string    `json:"fechavencimientofactura"`
	Saldo            float64   `json:"saldofactura"`
	Codigo           string    `json:"codigocliente"`
	Nombre           string    `json:"nombrecliente"`
	Correo           string    `json:"correocliente"`
	Telefono         string    `json:"telefonocliente"`
	Direccion        string    `json:"direccioncliente"`
	Dias             float64   `json:"dias"`
	Movimiento       float64   `json:"nummov"`
	DateVencimiento  time.Time `json:"datevencimiento,omitempty"`
	DateEmision      time.Time `json:"dateemision,omitempty"`
	ShortVencimiento string    `json:"shorvencimiento,omitempty"`
	ShortEmision     string    `json:"shortemision,omitempty"`
}

//SliceRegistros Alias para un slice de ObjetoCorreo
type SliceRegistros []ObjetoCorreo

//ObjetoMongo es la representacion de un registro en el almacen
type ObjetoMongo struct {
	Folio       float64   `bson:"foliofactura"`
	Serie       string    `bson:"seriefactura"`
	Vencimiento string    `bson:"fechavencimientofactura"`
	Saldo       float64   `bson:"saldofactura"`
	Codigo      string    `bson:"codigocliente"`
	Movimiento  float64   `bson:"nummov"`
	LastUpdate  time.Time `bson:"lastupdate"`
	Estatus     string    `bson:"estado"`
}

// CorreoPendiente Envio estructura que describe las credenciales de autenticacion de un correo.
type CorreoPendiente struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	Movimiento float64       `bson:"Movimiento"`
	LastUpdate time.Time     `bson:"LastUpdate"`
	Email      string        `bson:"Email"`
	Msg        string        `bson:"Msg"`
}

// Envio estructura que describe las credenciales de autenticacion de un correo.
type Envio struct {
	Correo   string
	Password string
	Servidor string
	Puerto   string
}

// Envios slice de clase envio
type Envios []Envio

// Contador contador
var Contador int64

/*
// ListaDistribucion lista de correos origen de distribucion
var ListaDistribucion = Envios{
	{Correo: "seguimientoaclientes1@outlook.com", Password: "23581321@1", Servidor: "smtp-mail.outlook.com", Puerto: "587"},
	{Correo: "seguimientoaclientes2@outlook.com", Password: "A2345678@", Servidor: "smtp-mail.outlook.com", Puerto: "587"},
	{Correo: "seguimientoaclientes3@outlook.com", Password: "A2345678@", Servidor: "smtp-mail.outlook.com", Puerto: "587"},
}
*/

/*
envio1@tubosyconexiones.mx   oxt{iHFMdCfT
envio2@tubosyconexiones.mx   oBxh}W;e?TV+
envio3@tubosyconexiones.mx   gE3[u73F3=^b

*/

// ExtractInformationElasticSearch Extrae la informacion de elastic y lo inserta en una estructura de facturas
//  Parametros:
// 		-resultados: resultados extraidos por elasticsearch
// 		-cliente: conexion hacia elasticsearc
//  Retorno:
// 		-clientes:  Coleccion de clientes con sus respectiva informacion
//  Autor: Ramon
//  Fecha: 10/03/2017ExtractInformationElasticSearch
//  Revision: 1.0
func ExtractInformationElasticSearch(resultados *elastic.SearchResult, client *elastic.Client) {
	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	sc := CalculaEnvio()
	fmt.Println("Tras obtener scheduler")
	if resultados.Hits.TotalHits > 0 {
		fmt.Println("Total de adeudos", resultados.Hits.TotalHits)
		i := 0
		notificaciones := 0
		recordatorios := 0
		for _, item := range resultados.Hits.Hits {
			var datosC ObjetoCorreo

			err := json.Unmarshal(*item.Source, &datosC)
			if err != nil {
				fmt.Println("Fallo de deserializacion -- ExtractInformationElasticSearch:114")
			}
			email := datosC.Correo
			// email = "test.miderp@hotmail.com"
			i++
			if datosC.Correo != "" && datosC.Saldo >= 1 {
				fmt.Println("Candidato: -------------->", email)
				fmt.Println("Candidato: ", datosC.Correo, "->", email)
				var objetoBD ObjetoMongo
				date := time.Now().Format(time.RFC3339Nano)
				t1, _ := time.Parse(time.RFC3339Nano, date)

				objetoBD.Folio = datosC.Folio
				objetoBD.Serie = datosC.Serie
				objetoBD.Saldo = datosC.Saldo
				objetoBD.Movimiento = datosC.Movimiento
				objetoBD.Codigo = datosC.Codigo
				objetoBD.Vencimiento = datosC.Vencimiento
				objetoBD.LastUpdate = t1

				var notificado bool
				var encontrado []ObjetoMongo

				notificado, encontrado = isNotified(objetoBD)
				fechaInicio, _ := time.Parse(time.RFC3339Nano, datosC.Vencimiento) //fecha de vencimiento
				fechaInicio = fechaInicio.AddDate(0, 0, 1)                         //factura vencida
				if t1.Before(fechaInicio) {                                        //hoy es antes del vencimiento?
					if !notificado { //no tiene registro de avisos
						horasAnticipadas := int(math.Floor(fechaInicio.Sub(t1).Hours())) //horas antes de vencer
						if horasAnticipadas < (sc.Aviso * 24) {                          //dentro del periodo de notificacion?
							objetoBD.Estatus = "Alerta"
							//correo:=datosC.Correo
							msg := GeneraObjetoCorreo(datosC, "")
							enviado := SendEmail(email, msg)
							if !enviado {
								InsertarCorreoPendiente(objetoBD.Movimiento, objetoBD.LastUpdate, email, msg)
							} else {
								InsertHistory(objetoBD)
							}
							notificaciones++
						} //en otro caso aun no es momento de avisar

					} else { //el candidato fuen notificado
						fmt.Println("Este elemento ya fue notificado por estar proximo a vencer: ", encontrado)
					}

				} else { //la fecha de vencimiento expiro Candidato a recordatorio Vencido
					objetoBD.Estatus = "Cobro"
					var tabla string
					listaVencidas := FacturaVencidaPorClienteID(datosC.Codigo)

					if len(listaVencidas) > 0 {
						tabla = construirTabla(listaVencidas)
					}

					if !notificado { //no ha sido notificado antes1
						msg := GeneraObjetoCorreo(datosC, tabla)
						enviado := SendEmail(email, msg)
						if !enviado {
							InsertarCorreoPendiente(objetoBD.Movimiento, objetoBD.LastUpdate, email, msg)
						} else {
							InsertHistory(objetoBD)
						}
						recordatorios++
					} else { //ya ha sido notificado antes
						horasPasadas := time.Since(fechaInicio).Hours()                             //cantidad de horas transcurridas
						diasPasados := horasPasadas / 24                                            //Dias vencidos
						elapsedTimes := int(math.Floor(diasPasados / float64(sc.CorreoFrecuencia))) //cantidad de periodos completos transcurridos
						diasRecorridoInicio := elapsedTimes * sc.CorreoFrecuencia                   //dias a partir de los periodos recoirridos
						fechaRecorridaInicio := fechaInicio.AddDate(0, 0, diasRecorridoInicio)      //fecha de inicio del periodo actual
						if encontrado[0].LastUpdate.After(fechaRecorridaInicio) {
							// se notifico entre el inicio y fin del periodo actual
							fmt.Println("Elemento encontrado: Ya se le ha enviado el correo correspondiente Alerta", encontrado)
						} else { // no se le ha notificado en este periodo
							fmt.Println("Insertar estatus cobro")
							msg := GeneraObjetoCorreo(datosC, tabla)
							enviado := SendEmail(email, msg)
							if !enviado {
								InsertarCorreoPendiente(objetoBD.Movimiento, objetoBD.LastUpdate, email, msg)
							} else {
								InsertHistory(objetoBD)
							}
							recordatorios++
						}
					}
				}
			}
		}
		fmt.Println("Notificaciones: ", notificaciones, " Recordatorios: ", recordatorios, "Total de Elementos: ", i)

		_, err := client.Flush().Index(DataCfg.InfElastic.NombreBase).Do(context.TODO())
		if err != nil {
			fmt.Println("Error al limpiar Objeto de consulta de elastic", err)
		}

	} else {
		fmt.Println("No se encontro algun registro")
	}
}

/*
 SendEmail : Envio de correos a una cuenta de cliente en especifico
 Parametros:
		-mail: cuenta a la cual se enviara el correo
 Retorno:
		-succes:  mensaje verdadero o falso de acuerdo a si fue satisfactorio o no el envio de correo
 Autor: Ramon
 Fecha: 10/03/2017isNotified
 Revision: 1.0
*/
func SendEmail(mail string, mensaje string) bool {

	DataCfg, err := MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	} else {
		if mail != "" {

			var turno int
			body := mensaje
			//turno := random() % 3
			//elemento := ListaDistribucion[turno]
			turno = random(0, len(DataCfg.InfCorreos.Usuario))

			Destino := fmt.Sprintf("%v", mail)
			uname := DataCfg.InfCorreos.Usuario[turno]
			pass := DataCfg.InfCorreos.Pass[turno]
			serv := DataCfg.InfCorreos.Servidor[turno]
			puerto := DataCfg.InfCorreos.Puerto[turno]
			servPort := fmt.Sprintf("%v:%v", serv, puerto)
			msg2 := "To: " + Destino + "\r\n" +
				"Content-type: text/html" + "\r\n" +
				"Subject: Cuenta vencida" + "\r\n\r\n" +
				body + "\r\n"

			/*
				Descontinuado---
				auth2 := smtp.PlainAuth(
					"",
					uname,
					pass,
					serv,
				)
			*/
			auth := LoginAuth(uname, pass)
			/*
				Pruebas---
				 msg := "To: " + "test.miderp@hotmail.com" + "\r\n" +
				 	"Content-type: text/html" + "\r\n" +
				 	"Subject: Cuenta vencida" + "\r\n\r\n" +
				 	body + "\r\n"
				 // Set up authentication information.
				 auth := smtp.PlainAuth(
				 	"",
				 	"test.miderp@hotmail.com",
				 	"D3m0st3n3s",
				 	"smtp-mail.outlook.com",
				 )
			*/
			//fmt.Println("Auth->:  ", auth, ".  Server->: ", serv, " ServerPort->: ", servPort, " Destino>: ", Destino)
			/*
				 Descontinuado...
				err := smtp.SendMail(
					servPort,
					auth2,
					uname,
					// []string{"test.miderp@hotmail.com", Destino},
					// []byte(msg2),
					//[]string{"test.miderp@hotmail.com", Destino},
					[]string{Destino},
					[]byte(msg2),
				)
			*/
			err := smtp.SendMail(
				servPort,
				auth,
				uname,
				[]string{Destino},
				[]byte(msg2))

			if err != nil {
				fmt.Println("Error al intentar enviar el mail: ", err)
				return false
			}
			return true

		} else {
			fmt.Println("No existe ningun correo.")
		}
		return false
	}
	return false
}

type loginAuth struct {
	username, password string
}

func LoginAuth(username, password string) smtp.Auth {
	return &loginAuth{username, password}
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("Unkown fromServer")
		}
	}
	return nil, nil
}

//CalculaEnvio devuelve el scheduler asignado en la aplicacion
func CalculaEnvio() mongoconn.Scheduler {
	var Schedul mongoconn.Scheduler
	Schedul = mongoconn.GetConfiguracion()
	return Schedul
}

func isNotified(consultaObjeto ObjetoMongo) (bool, []ObjetoMongo) {
	var result []ObjetoMongo
	notificado := false
	s := mongoconn.ConectaMgo()
	defer s.Close()

	d := s.DB("gestioncobranza")
	err := d.Login("consultas", "Hola1234")
	if err != nil {
		fmt.Println("No esta autorizado para conectarse a la base de datos.", err)
		fmt.Println(err)
	} else {
		c := d.C("historialcorreo")
		query := bson.M{"foliofactura": consultaObjeto.Folio, "seriefactura": consultaObjeto.Serie}

		err := c.Find(query).
			Sort("-lastupdate").
			All(&result)
		if err != nil {
			fmt.Println("Error: ", err)
			notificado = false
		}

		fmt.Println("Elementos: encontrados", len(result))
		if len(result) == 0 {
			notificado = false
		} else {
			notificado = true
		}
	}

	// // return true, result
	return notificado, result
}

func InsertHistory(consultaObjeto ObjetoMongo) bool {
	insertado := false
	s := mongoconn.ConectaMgo()
	defer s.Close()

	d := s.DB("gestioncobranza")
	err := d.Login("consultas", "Hola1234")
	if err != nil {
		fmt.Println("No esta autorizado para conectarse a la base de datos.", err)
		insertado = false
	} else {
		c := d.C("historialcorreo")
		auxiliar := consultaObjeto
		auxiliar.LastUpdate = time.Now()
		_, err := c.Upsert(bson.M{"foliofactura": consultaObjeto.Folio, "seriefactura": consultaObjeto.Serie}, auxiliar)
		if err != nil {
			fmt.Println("Error al insertar o actualizar objeto")
			insertado = false
		} else {
			insertado = true
		}
	}

	s.Close()
	return insertado
}

func GeneraObjetoCorreo(datos ObjetoCorreo, tabla string) string {
	var correo string
	correo = `<meta charset="utf-8"/>`
	correo = correo + fmt.Sprintf("Estimado cliente:  %s, <br>", datos.Nombre)
	correo = correo + fmt.Sprintf("Se le comunica que la factura con los siguientes datos:  <br>")
	correo = correo + fmt.Sprintf("<b>Serie</b>: %s <br>", datos.Serie)
	correo = correo + fmt.Sprintf("<b>Folio</b>: %d<br>", int(datos.Folio))
	correo = correo + fmt.Sprintf("<b>Monto</b>: $%.2f <br>", datos.Saldo)
	vencimiento, _ := time.Parse(time.RFC3339, datos.Vencimiento)
	emision, _ := time.Parse(time.RFC3339, datos.Emision)
	correo = correo + fmt.Sprintf("<b>Emision</b>: %s <br>", emision.Format("02/01/2006"))
	correo = correo + fmt.Sprintf("<b>Vencimiento</b>: %s <br>", vencimiento.Format("02/01/2006"))
	var estado string
	if datos.Dias > 0 {
		estado = "Por Vencer"
	} else {
		estado = "Vencida"
	}
	correo = correo + fmt.Sprintf("Se encuentra con un Estatus: <b>%s</b>.  <br>", estado)
	correo = correo + tabla
	correo = correo + fmt.Sprintf("Este correo es informativo. Para resolver cualquier duda o aclaracion no dude en llamar<br>")
	correo = correo + fmt.Sprintf("a su agente de ventas asignado.<br>")
	correo = correo + fmt.Sprintf("------  <br>")
	correo = correo + fmt.Sprintf("Tubos y Conexiones.<br>")

	return correo
}

func FacturaVencidaPorClienteID(id string) []ObjetoCorreo {
	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	var retorno []ObjetoCorreo
	var docs *elastic.SearchResult
	var queryString string
	queryString = id

	cliente := elasticconnector.ConectarElasticSearch(DataCfg.InfElastic.Servidor, DataCfg.InfElastic.Puerto)

	query := elastic.NewQueryStringQuery(queryString)
	query = query.Field("codigocliente")
	query = query.DefaultOperator("AND")

	docs, err = cliente.Search().Index(DataCfg.InfElastic.NombreBase).Type(elasticconnector.COLECCIONELASTICSEARCH).Query(query).Do(context.TODO())

	if err != nil {
		fmt.Println("No se pudo ejecutar la busqueda en elasticsearch")
	}
	for _, item := range docs.Hits.Hits {
		var datosC ObjetoCorreo
		err := json.Unmarshal(*item.Source, &datosC)
		if err != nil {
			fmt.Println("Fallo de desserealizacion")
		}

		var fechaV time.Time
		fechaV, err = time.Parse(time.RFC3339Nano, datosC.Vencimiento)
		if err != nil {
			fmt.Println("No se pudo convertir la hora")
		}
		if time.Now().After(fechaV) {
			retorno = append(retorno, datosC)
		}

	}

	return retorno
}

func construirTabla(datosVencidos []ObjetoCorreo) string {
	var tabla string
	var MontoTotal float64
	MontoTotal = 0
	vencidosmayorquecero := 0
	for _, item := range datosVencidos {
		if item.Saldo > 0 {
			vencidosmayorquecero = vencidosmayorquecero + 1
		}
	}
	if vencidosmayorquecero > 0 {
		estilo := `<style>
						table {  width: 100%;   
								border: 1px;   
								font-family: 'Lucida Sans Unicode', 
								'Lucida Grande', Sans-Serif;  font-size: 12px;   
								text-align: left;    
								border-collapse: collapse;
							 } 
						th {     
								font-size: 13px;
								font-weight: normal;     
								padding: 8px;     
								background: #b9c9fe;    
								border-top: 4px solid #aabcfe;    
								border-bottom: 1px solid #fff; 
								color: #039; 
							}
							td {    
								padding: 8px;     
								background: #e8edff;     
								border-bottom: 1px solid #fff;  
								color: #669;    
								border-top: 1px solid transparent;
							}
							td.dineros{
								text-align:rigth;
							}
				</style>`
		tabla = estilo + `<meta charset="utf-8"/>` + "<br>La siguiente es una lista con los datos de las facturas que se encuentran con estado vencido. <br>"
		tabla = tabla + fmt.Sprintf(`<table  class='table table-hover'>`)
		tabla = tabla + fmt.Sprintf("<tr>")
		tabla = tabla + fmt.Sprintf("<th>")
		tabla = tabla + fmt.Sprintf("Serie")
		tabla = tabla + fmt.Sprintf("</th>")
		tabla = tabla + fmt.Sprintf("<th>")
		tabla = tabla + fmt.Sprintf("Folio")
		tabla = tabla + fmt.Sprintf("</th>")
		tabla = tabla + fmt.Sprintf("<th>")
		tabla = tabla + fmt.Sprintf("Monto")
		tabla = tabla + fmt.Sprintf("</th>")
		tabla = tabla + fmt.Sprintf("<th>")
		tabla = tabla + fmt.Sprintf("Emisi&oacute;n")
		tabla = tabla + fmt.Sprintf("</th>")
		tabla = tabla + fmt.Sprintf("<th>")
		tabla = tabla + fmt.Sprintf("Vencimiento")
		tabla = tabla + fmt.Sprintf("</th>")

		tabla = tabla + fmt.Sprintf("</tr>\n")
		for _, item := range datosVencidos {
			if item.Saldo > 0 {
				tabla = tabla + fmt.Sprintf("<tr>")
				tabla = tabla + fmt.Sprintf("<td>")
				tabla = tabla + fmt.Sprintf("%s", item.Serie)
				tabla = tabla + fmt.Sprintf("</td>")
				tabla = tabla + fmt.Sprintf("<td>")
				tabla = tabla + fmt.Sprintf("%d", int(item.Folio))
				tabla = tabla + fmt.Sprintf("</td>")

				tabla = tabla + fmt.Sprintf("<td style='text-align: right;'>")
				tabla = tabla + fmt.Sprintf("$%.2f", item.Saldo)
				tabla = tabla + fmt.Sprintf("</td>")
				tabla = tabla + fmt.Sprintf("<td>")
				t1, err := time.Parse(time.RFC3339, item.Emision)
				if err != nil {
					fmt.Println("Error al pasear hora emision")
				}
				tabla = tabla + fmt.Sprintf("%s", t1.Format("02/01/2006"))
				tabla = tabla + fmt.Sprintf("</td>")
				tabla = tabla + fmt.Sprintf("<td>")
				t1, _ = time.Parse(time.RFC3339, item.Emision)
				if err != nil {
					fmt.Println("Error alk pasear hora emision")
				}
				tabla = tabla + fmt.Sprintf("%s", t1.Format("02/  01/2006"))
				tabla = tabla + fmt.Sprintf("</td>")
				tabla = tabla + fmt.Sprintf("<td style='text-align: right;'>")
				tabla = tabla + fmt.Sprintf("</td>")

				tabla = tabla + fmt.Sprintf("</tr>\n")
				MontoTotal += item.Saldo
			}
		}
		tabla = tabla + fmt.Sprintf("</table><br>")
		tabla = tabla + fmt.Sprintf("Por un monto total de $%.2f.", MontoTotal)
	}

	return tabla
}

func (p SliceRegistros) Less(i, j int) bool {
	return p[i].DateVencimiento.Before(p[j].DateVencimiento)
}

func (p SliceRegistros) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p SliceRegistros) Len() int {
	return len(p)
}

//
func random(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	//rand.Seed(time.Now().Unix())
	//return rand.Intn(99999 - 0)
	return randInt(min, max)
}

func randInt(min, max int) int {
	return min + rand.Intn(max-min)
}

func InsertarCorreoPendiente(Movimiento float64, LastUpdate time.Time, email string, msg string) {
	var correopendiente CorreoPendiente
	correopendiente.ID = bson.NewObjectId()
	correopendiente.Movimiento = Movimiento
	correopendiente.LastUpdate = LastUpdate
	correopendiente.Email = email
	correopendiente.Msg = msg

	s := mongoconn.ConectaMgo()
	d := s.DB("gestioncobranza")
	err := d.Login("consultas", "Hola1234")

	if err != nil {
		fmt.Println("No esta autorizado para conectarse a la base de datos.", err)
		fmt.Println("Err:(", err)
	} else {
		fmt.Println("Se ha autenticado al usuario:)")
		c := d.C("pendientes")
		err := c.Insert(correopendiente)
		if err != nil {
			fmt.Println("Err:(", err)
		}
		fmt.Println("Insercion Correcta :)")

		d.Logout()
	}

	s.Close()
	fmt.Println("aqui se debe insertar en mongo un correo")
}

func IngresarTodos(resultados *elastic.SearchResult, client *elastic.Client) {

	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	if resultados.Hits.TotalHits > 0 {
		fmt.Println("Total de adeudos", resultados.Hits.TotalHits)
		i := 0
		notificaciones := 0
		recordatorios := 0
		for _, item := range resultados.Hits.Hits {
			var datosC ObjetoCorreo
			err := json.Unmarshal(*item.Source, &datosC)
			if err != nil {
				fmt.Println("Fallo de deserializacion: /ServicioCorreo/correo.go:587")
			}
			i++
			if datosC.Correo != "" && datosC.Saldo >= 1 {
				var objetoBD ObjetoMongo
				date := time.Now().Format(time.RFC3339Nano)
				t1, _ := time.Parse(time.RFC3339Nano, date)
				objetoBD.Folio = datosC.Folio
				objetoBD.Serie = datosC.Serie
				objetoBD.Saldo = datosC.Saldo
				objetoBD.Movimiento = datosC.Movimiento
				objetoBD.Codigo = datosC.Codigo
				objetoBD.Vencimiento = datosC.Vencimiento
				objetoBD.LastUpdate = t1
				InsertHistory(objetoBD)
			}
		}
		fmt.Println("Notificaciones: ", notificaciones, " Recordatorios: ", recordatorios, "Total de Elementos: ", i)
		_, err := client.Flush().Index(DataCfg.InfElastic.NombreBase).Do(context.TODO())
		if err != nil {
			fmt.Println("Error al limpiar Objeto de consulta de elastic", err)
		}

	} else {
		fmt.Println("No se encontro algun registro")
	}
}
