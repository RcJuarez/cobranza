package controllers

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"../ServicioCorreo/Conexiones"
)

var tmpl_index = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Plantilla/sidebar.html",
	"Vistas/Parciales/Plantilla/leftuppermenu.html",
	"Vistas/Parciales/Paginas/Dashboard/content.html",
))

var result mongoconn.Scheduler

func SetView(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		fmt.Println("Enviando config")
		result = mongoconn.GetConfiguracion()
		fmt.Println("resultado config: ", result)
		tmpl_index.ExecuteTemplate(w, "index_layout", result)

	}
}

//Set funcion que fija los parametros de Scheduler
func Set(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Inicia guardado...")
	if r.Method == "POST" {
		//Verifiacr captura exitosa
		Exito := true
		//Data de tipo estructura
		var data mongoconn.Scheduler
		//Cacho variables y Construyo datos a estructura
		r.ParseForm()
		fmt.Println("--------------Setting Scheduler--------------")
		fmt.Println(r.Form)
		fmt.Println("---------------------------------------------")
		frecuencia, err := strconv.Atoi(r.PostFormValue("frecuencia"))
		if err != nil {
			fmt.Println("--------------Wrong Frecuencia---------------")
			fmt.Println(r.Form)
			fmt.Println("---------------------------------------------")
			Exito = false
		}
		data.CorreoFrecuencia = frecuencia
		antesde, err := strconv.Atoi(r.PostFormValue("antesde"))
		if err != nil {
			fmt.Println("--------------Wrong AntesDe------------------")
			fmt.Println(r.Form)
			fmt.Println("---------------------------------------------")
			Exito = false
		}

		data.Aviso = antesde

		// domingo := r.FormValue("domingo")
		// lunes := r.PostFormValue("lunes")
		// martes := r.PostFormValue("martes")
		// miercoles := r.PostFormValue("miercoles")
		// jueves := r.PostFormValue("jueves")
		// viernes := r.PostFormValue("viernes")
		// sabado := r.PostFormValue("sabado")

		// fmt.Println(domingo, lunes, martes, miercoles, jueves, viernes, sabado)

		// data.Lunes, err = strconv.ParseBool(lunes)
		// if err != nil {
		// 	fmt.Println("Error al obtener la bandera de dia lunes")
		// 	Exito = false
		// }

		// data.Martes, err = strconv.ParseBool(martes)
		// if err != nil {
		// 	fmt.Println("Error al obtener la bandera de dia martes")
		// 	Exito = false
		// }
		// data.Miercoles, err = strconv.ParseBool(miercoles)
		// if err != nil {
		// 	fmt.Println("Error al obtener la bandera de dia miercoles")
		// 	Exito = false
		// }
		// data.Jueves, err = strconv.ParseBool(jueves)
		// if err != nil {
		// 	fmt.Println("Error al obtener la bandera de dia jueves")
		// 	Exito = false
		// }
		// data.Viernes, err = strconv.ParseBool(viernes)
		// if err != nil {
		// 	fmt.Println("Error al obtener la bandera de dia viernes")
		// 	Exito = false
		// }
		// data.Sabado, err = strconv.ParseBool(sabado)
		// if err != nil {
		// 	fmt.Println("Error al obtener la bandera de dia sabado")
		// 	Exito = false
		// }
		// data.Domingo, err = strconv.ParseBool(domingo)
		// if err != nil {
		// 	fmt.Println("Error al obtener la bandera de dia domingo")
		// 	Exito = false
		// }

		if Exito {
			//Elimino
			mongoconn.DelConfiguracion()
			//Inserto datos a mongo
			result := mongoconn.SetConfiguracion(data)
			fmt.Println("Resultado de Insercion: ", result)
			fmt.Fprintf(w, `<script>alert('¡Insertado correctamente!'); alertify.success("Insercion Exitosa")</script>`)
			fmt.Fprintf(w, `<script>$("#contenedor").html(%d);</script>`, frecuencia)
		} else {
			fmt.Fprintf(w, `<script>alert('¡No insertado, se detectaron errores, se conserva el valor anterior!');</script>`)
		}
	}
}
