package controllers

import (
	"fmt"
	"html/template"
	"net/http"
)

var tmpl_index2 = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Plantilla/sidebar.html",
	"Vistas/Parciales/Plantilla/leftuppermenu.html",
	"Vistas/Parciales/Paginas/Indice/index.html",
))

func IndiceGeneral(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		fmt.Println("Controllers: Indice.Index.html")
		tmpl_index2.ExecuteTemplate(w, "index_layout", nil)

	}
}
