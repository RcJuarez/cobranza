package VistaVencidas

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	elastic "gopkg.in/olivere/elastic.v5"

	"math"

	"time"

	"sort"

	"../../ServicioCorreo/Correo"
	"../../ServicioCorreo/Elastic"
	"../../Variables"
	"github.com/gorilla/mux"
)

//TmplIndex archivos por compilar para generar el objeto de correo
var TmplIndex = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/searchheader.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Plantilla/sidebar.html",
	"Vistas/Parciales/Plantilla/leftuppermenu.html",
	"Vistas/Parciales/Paginas/FacturasVencidas/content.html",
))

var numero_registros int
var lim int = 15 // tamaño de la muestra
var totalpages float64
var listaSlice correo.SliceRegistros
var lista []correo.ObjetoCorreo
var tmp_paginacion string
var templateResultados string
var paginaactual int

func Totalpaginas() {
	totalpages = math.Ceil(float64(numero_registros) / float64(lim))
}

//ShowList compila el template que mostrara la tabla de fascturas vencidas
func ShowList(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		lista = nil
		tmp_paginacion = ``
		templateResultados = ``

		lista, numero_registros = GenerateList()
		listaSlice := correo.SliceRegistros(lista)
		sort.Sort(listaSlice)
		lista = listaSlice

		Totalpaginas()

		variables_mux := mux.Vars(r)
		paginaactual_str := variables_mux["p"]
		paginaactual, _ = strconv.Atoi(paginaactual_str)

		templateResultados = ConstruirTabla(paginaactual)
		tmp_paginacion = ConstruirPaginacion(paginaactual)

		fmt.Println("pagina actual", paginaactual)

		TmplIndex.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"lista":         lista,
			"PaginacionT":   template.HTML(tmp_paginacion),
			"TemplateTabla": template.HTML(templateResultados),
		})

		// TmplIndex.ExecuteTemplate(w, "index_layout", lista)

		fmt.Println("TOTAL DE DATOS LEIDOS", len(lista), " TOTAL VENCIDAS", numero_registros)

	} //else if r.Method == "POST" {
	// 	r.ParseForm()
	// 	busqueda := r.FormValue("filtroBusqueda")
	// 	fmt.Println("cosa a buscar", busqueda)
	// 	cliente := elasticconnector.ConectarElasticSearch(elasticconnector.IP)

	// 	html := GetOverdueBillsByClient2(cliente, busqueda)
	// 	fmt.Fprintln(w, html)

	// 	_, err := cliente.Flush().Index(elasticconnector.INDICEELASTICSEARCH).Do(context.TODO())
	// 	if err != nil {
	// 		fmt.Println("Error al limpiar Objeto de consulta de elastic", err)
	// 	}

	// }
}
func SearchClientBills(w http.ResponseWriter, r *http.Request) {
	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	if r.Method == "POST" {
		r.ParseForm()
		busqueda := r.FormValue("cliente")
		fmt.Println("cosa a buscar", busqueda)
		cliente := elasticconnector.ConectarElasticSearch(DataCfg.InfElastic.Servidor, DataCfg.InfElastic.Puerto)
		html := GetOverdueBillsByClient2(cliente, busqueda)
		fmt.Fprintln(w, html)
		_, err := cliente.Flush().Index(DataCfg.InfElastic.NombreBase).Do(context.TODO())
		if err != nil {
			fmt.Println("Error al limpiar Objeto de consulta de elastic", err)
		}

	}
}

func ShowListByClient(w http.ResponseWriter, r *http.Request) {
	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	if r.Method == "POST" {
		cliente := elasticconnector.ConectarElasticSearch(DataCfg.InfElastic.Servidor, DataCfg.InfElastic.Puerto)
		r.ParseForm()
		busqueda := r.FormValue("cliente")

		html := GetOverdueBillsByClient(cliente, busqueda)
		fmt.Fprintln(w, html)
		_, err := cliente.Flush().Index(DataCfg.InfElastic.NombreBase).Do(context.TODO())
		if err != nil {
			fmt.Println("Error al limpiar Objeto de consulta de elastic", err)
		}
	}
}

//GenerateList funcion que regresa la lista de objetos a pintarse en elastic
func GenerateList() ([]correo.ObjetoCorreo, int) {
	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	var retorno []correo.ObjetoCorreo
	cliente := elasticconnector.ConectarElasticSearch(DataCfg.InfElastic.Servidor, DataCfg.InfElastic.Puerto)
	allresults := elasticconnector.GetAllElasticSearch(DataCfg.InfElastic.NombreBase, cliente)
	totalHits := int64(allresults.Hits.TotalHits)
	fmt.Println("Total de datos leidos ", totalHits)

	resultado := GetRangeElasticSearch(cliente, 1)
	cosciente := float64(totalHits)
	cosciente /= float64(lim)
	paginasTotales := int(math.Ceil(cosciente))
	i := 0
	fmt.Println("Total de paginas", paginasTotales)
	if totalHits > 0 {
		for _, item := range resultado.Hits.Hits {
			var datosC correo.ObjetoCorreo
			err := json.Unmarshal(*item.Source, &datosC)
			if err != nil {
				fmt.Println("GenerateList")
				fmt.Println("Fallo de deserializacion...: Controladores/VistaVencidas/Vencidas.go:153", err)
			}
			datosC.Saldo = float64(int(datosC.Saldo*100)) / 100
			datosC.DateVencimiento, _ = time.Parse(time.RFC3339Nano, datosC.Vencimiento)
			datosC.DateEmision, _ = time.Parse(time.RFC3339Nano, datosC.Emision)
			datosC.ShortVencimiento = datosC.DateVencimiento.Format("02/01/2006")
			datosC.ShortEmision = datosC.DateEmision.Format("02/01/2006")

			if datosC.Correo != "" && datosC.Saldo > 0 && time.Now().After(datosC.DateVencimiento) {
				retorno = append(retorno, datosC)
				i++
			}
		}
	}
	// totalLeidos := resultado.Hits.TotalHits
	fmt.Println("el total de documentos vencidos es: ", i)

	_, err = cliente.Flush().Index(DataCfg.InfElastic.NombreBase).Do(context.TODO())
	if err != nil {
		fmt.Println("Error al limpiar Objeto de consulta de elastic", err)
	}
	return retorno, i
}

//GetRangeElasticSearch obtiene los datos de elastic
func GetRangeElasticSearch(client *elastic.Client, pagina int) *elastic.SearchResult {

	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	QueryTodos := elastic.NewMatchAllQuery()
	desde := (pagina - 1) * lim
	docs, err := client.Search().
		Index(DataCfg.InfElastic.NombreBase).
		Type(elasticconnector.COLECCIONELASTICSEARCH).
		Query(QueryTodos).
		Sort("dias", true).
		From(desde).
		Size(elasticconnector.TOTALRESULTADOSDEVUELTOS).
		Do(context.TODO())
	if err != nil {
		fmt.Println("Error al realizar la busqueda")
		fmt.Println("Panic Call: ", err)
	}
	return docs
}

//GetOverdueBillsByClient obtiene facturas vencidas por cliente
func GetOverdueBillsByClient(client *elastic.Client, numcliente string) string {

	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	//indice gestioncobranza obtener:
	//todos aquellos registros donde
	//correocliente: !="" y != nil
	//saldofactura >=1
	var retorno []correo.ObjetoCorreo
	consulta := fmt.Sprintf(`saldofactura:>0 AND codigocliente:%s`, numcliente)
	fmt.Println("Consulta querystringquery: ", consulta)
	querySq := elastic.NewQueryStringQuery(consulta)

	docs, err := client.Search().
		Index(DataCfg.InfElastic.NombreBase).
		Type(elasticconnector.COLECCIONELASTICSEARCH).
		Query(querySq).
		From(0).
		Size(elasticconnector.TOTALRESULTADOSDEVUELTOS).
		Do(context.TODO())
	if err != nil {
		fmt.Println("Error al realizar la busqueda")
		fmt.Println("Panic Call: ", err)
	}
	i := 0
	if docs.Hits.TotalHits > 0 {

		for _, item := range docs.Hits.Hits {
			var datosC correo.ObjetoCorreo
			err := json.Unmarshal(*item.Source, &datosC)
			if err != nil {
				fmt.Println("Fallo de desserealizacion")
			}
			datosC.Saldo = float64(int(datosC.Saldo*100)) / 100
			datosC.DateVencimiento, _ = time.Parse(time.RFC3339Nano, datosC.Vencimiento)
			datosC.DateEmision, _ = time.Parse(time.RFC3339Nano, datosC.Emision)
			datosC.ShortVencimiento = datosC.DateVencimiento.Format("02/01/2006")
			datosC.ShortEmision = datosC.DateEmision.Format("02/01/2006")

			if datosC.Correo != "" && datosC.Saldo > 0 && time.Now().After(datosC.DateVencimiento) {
				retorno = append(retorno, datosC)
				i++
			}
		}
	}
	if i > 0 {
		var sliceCorreo correo.SliceRegistros
		sliceCorreo = retorno
		sort.Sort(sliceCorreo)
		retorno = sliceCorreo
		consulta = `<table class="table table-stripped">`
		consulta += fmt.Sprintf(`<thead>`)
		consulta += fmt.Sprintf(`<th>Serie/Folio</th>`)
		consulta += fmt.Sprintf(`<th>Movimiento</th>`)
		consulta += fmt.Sprintf(`<th>Emision</th>`)
		consulta += fmt.Sprintf(`<th>Vencimiento</th>`)
		consulta += fmt.Sprintf(`<th>Saldo</th>`)
		consulta += fmt.Sprintf(`</thead>`)
		consulta += `<tbody>`
		for _, valor := range retorno {
			consulta += fmt.Sprintf(`<tr>`)
			consulta += fmt.Sprintf(`<td>%s/%.0f</td>`, valor.Serie, valor.Folio)
			consulta += fmt.Sprintf(`<td>%.0f</td>`, valor.Movimiento)
			consulta += fmt.Sprintf(`<td>%s</td>`, valor.ShortEmision)
			consulta += fmt.Sprintf(`<td>%s</td>`, valor.ShortVencimiento)
			consulta += fmt.Sprintf(`<td class="dineros">%.2f</td>`, valor.Saldo)
			consulta += fmt.Sprintf(`</tr>`)
		}
		consulta += `</tbody>`
		consulta += `</table>`
	} else {
		consulta = ""
	}

	return consulta
}

func ConstruirPaginacion(pagina int) string {

	var tmp_paginacion string

	if pagina == 0 {

		tmp_paginacion += `
				<nav aria-label="Page navigation">
	  					<ul class="pagination">
	    				<li>
	      					<a href="/vencidas/1" aria-label="Primera">
	        				<span aria-hidden="true">&laquo;</span>
	      					</a>
	    				</li>`

		for i := 1; float64(i) <= totalpages; i++ {

			if float64(i) == 1 {
				tmp_paginacion = tmp_paginacion + `<li class="active"><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			} else if i > 1 && i < 11 {
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			} else if i > 11 && float64(i) == totalpages {
				tmp_paginacion = tmp_paginacion + `<li><span aria-hidden="true">...</span></li><li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			}

		}

		tmp_paginacion = tmp_paginacion + `<li>
								<a href="/vencidas/` + strconv.Itoa(int(totalpages)) + `" aria-label="Ultima">
	        				<span aria-hidden="true">&raquo;</span>
	      					</a>
	    				</li>
	  				</ul>
				</nav>
			`

	} else {

		if pagina != 0 {
			pagina = pagina - 1
		}

		tmp_paginacion = tmp_paginacion + `
					<nav aria-label="Page navigation">
		  					<ul class="pagination">
		    				<li>
		      					<a href="/vencidas/1" aria-label="Primera">
		        				<span aria-hidden="true">&laquo;</span>
		      					</a>
		    				</li>`

		for i := 1; i <= int(totalpages); i++ {

			if paginaactual > 6 && i == 1 {
				tmp_paginacion = tmp_paginacion + `
									<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>
									<li>
										<span aria-hidden="true">...</span>
									</li>
									`
			}

			switch {

			case paginaactual == i:
				if paginaactual == 1 {
					tmp_paginacion = tmp_paginacion + `<li class="active"><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				} else {
					tmp_paginacion = tmp_paginacion + `<li class="active"><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				}
				break
			case paginaactual == i-1:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break

			case paginaactual == i-2:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break

			case paginaactual == i-3:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break
			case paginaactual == i-4:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break
			case paginaactual == i-5:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break

			case paginaactual == i+1:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break

			case paginaactual == i+2:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break

			case paginaactual == i+3:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break
			case paginaactual == i+4:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break
			case paginaactual == i+5:
				tmp_paginacion = tmp_paginacion + `<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
				break
			}

			if i > paginaactual+6 && int(totalpages) == i {
				tmp_paginacion = tmp_paginacion + `
										<li>
							  				<span aria-hidden="true">...</span>
										</li>
									<li><a href="/vencidas/` + strconv.Itoa(i) + `">` + strconv.Itoa(i) + `</a></li>`
			}

		}

		tmp_paginacion = tmp_paginacion + `<li>
									<a href="/vencidas/` + strconv.Itoa(int(totalpages)) + `" aria-label="Ultima">
		        				<span aria-hidden="true">&raquo;</span>
		      					</a>
		    				</li>
		  				</ul>
					</nav>
				`

	}

	return tmp_paginacion
}

func ConstruirTabla(pagina int) string {

	var tmp_tabla string

	var inicioSlice int
	var finSlice int

	if pagina != 0 {
		pagina -= 1
	}

	inicioSlice = pagina * lim
	finSlice = (pagina + 1) * lim
	fmt.Println("---------------------------------------")
	fmt.Println("---------------------------------------")
	fmt.Println("---------------------------------------")
	fmt.Printf("inicio %v y fin %v.\n", inicioSlice, finSlice)
	fmt.Printf("numero_registros %v <= %v lim ", numero_registros, lim)
	fmt.Printf("len lista %v \n", len(lista))
	fmt.Println("---------------------------------------")
	fmt.Println("---------------------------------------")
	fmt.Println("---------------------------------------")

	if numero_registros <= lim {
		for _, v := range lista[inicioSlice:finSlice] {
			tmp_tabla += fmt.Sprintf(`
			
			<tr id="%v" ondblclick="showListOverdueByClient('%v', '%v', '%v',this)">
				<td>
                	<input type="hidden" value="%v" id="Serie%v">
                	<input type="hidden" value="%v" id="Folio%v"> %v/%v
            	</td>
            	
            	<td>
                	<input type="hidden" value="%v" id="Movimiento%v"> %v
            	</td>
            	<td>
                	<input type="hidden" value="%v" id="Cliente%v"> %v
            	</td>
            	<td>
	                <input type="hidden" value="%v" id="Nombre%v">
	                <input type="hidden" value="%v" id="Codigo%v"> %v
	            </td>
            	
	            <td>
	                <input type="hidden" value="%v" id="Correo%v"> %v
	            </td>
	            <td>
	                <input type="hidden" value="%v" id="Emision%v"> %v
	            </td>

	            <td>
	                <input type="hidden" value="%v" id="Codigo%v"> %v
	            </td>
	            <td class="dineros">
	                <input type="hidden" value="%v" id="Saldo%v"> %v
	            </td>
	            <td>
	                <input type="button" value="Enviar" onclick="enviarCorreo('%v', '%v', '%v', '%v', '%v', '%v', '%v',this)">
	            </td>
	        </tr>
	            
			`, v.Codigo, v.Codigo, v.Nombre, v.Correo,
				v.Serie, v.Movimiento,
				v.Folio, v.Movimiento, v.Serie, v.Folio,
				v.Movimiento, v.Movimiento, v.Movimiento,
				v.Codigo, v.Movimiento, v.Codigo,
				v.Nombre, v.Movimiento,
				v.Codigo, v.Movimiento, v.Nombre,
				v.Correo, v.Movimiento, v.Correo,
				v.Emision, v.Movimiento, v.ShortEmision,
				v.Vencimiento, v.Movimiento, v.ShortVencimiento,
				v.Saldo, v.Movimiento, v.Saldo,
				v.Nombre, v.Serie, v.Folio, v.ShortEmision, v.ShortVencimiento, v.Saldo, v.Correo)

		}
	} else if numero_registros >= lim {

		if finSlice >= numero_registros {
			finSlice = numero_registros
		}

		for _, v := range lista[inicioSlice:finSlice] {
			tmp_tabla += fmt.Sprintf(`
			
			<tr id="%v" ondblclick="showListOverdueByClient('%v', '%v', '%v',this)">
				<td>
                	<input type="hidden" value="%v" id="Serie%v">
                	<input type="hidden" value="%v" id="Folio%v"> %v/%v
            	</td>
            	
            	<td>
                	<input type="hidden" value="%v" id="Movimiento%v"> %v
            	</td>
            	<td>
                	<input type="hidden" value="%v" id="Cliente%v"> %v
            	</td>
            	<td>
	                <input type="hidden" value="%v" id="Nombre%v">
	                <input type="hidden" value="%v" id="Codigo%v"> %v
	            </td>
            	
	            <td>
	                <input type="hidden" value="%v" id="Correo%v"> %v
	            </td>
	            <td>
	                <input type="hidden" value="%v" id="Emision%v"> %v
	            </td>

	            <td>
	                <input type="hidden" value="%v" id="Codigo%v"> %v
	            </td>
	            <td class="dineros">
	                <input type="hidden" value="%v" id="Saldo%v"> %v
	            </td>
	            <td>
	                <input type="button" value="Enviar" onclick="enviarCorreo('%v', '%v', '%v', '%v', '%v', '%v','%v',this)">
	            </td>
	        </tr>
	            
			`, v.Codigo, v.Codigo, v.Nombre, v.Correo,
				v.Serie, v.Movimiento,
				v.Folio, v.Movimiento, v.Serie, v.Folio,
				v.Movimiento, v.Movimiento, v.Movimiento,
				v.Codigo, v.Movimiento, v.Codigo,
				v.Nombre, v.Movimiento,
				v.Codigo, v.Movimiento, v.Nombre,
				v.Correo, v.Movimiento, v.Correo,
				v.Emision, v.Movimiento, v.ShortEmision,
				v.Vencimiento, v.Movimiento, v.ShortVencimiento,
				v.Saldo, v.Movimiento, v.Saldo,
				v.Nombre, v.Serie, v.Folio, v.ShortEmision, v.ShortVencimiento, v.Saldo, v.Correo)

		}
	}

	return tmp_tabla
}

//GetOverdueBillsByClient2 obtiene facturas vencidas por cliente
func GetOverdueBillsByClient2(client *elastic.Client, numcliente string) string {

	//Extraer la informacion de configuracino
	var DataCfg, err = MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	}

	//indice gestioncobranza obtener:
	//todos aquellos registros donde
	//correocliente: !="" y != nil
	//saldofactura >=1
	var retorno []correo.ObjetoCorreo

	consulta := fmt.Sprintf(`saldofactura:>0 AND (codigocliente:%s OR nombrecliente:"%s")`, numcliente, numcliente)
	fmt.Println("Consulta querystringquery: ", consulta)
	querySq := elastic.NewQueryStringQuery(consulta)

	docs, err := client.Search().
		Index(DataCfg.InfElastic.NombreBase).
		Type(elasticconnector.COLECCIONELASTICSEARCH).
		Query(querySq).
		From(0).
		Size(elasticconnector.TOTALRESULTADOSDEVUELTOS).
		Do(context.TODO())
	if err != nil {
		fmt.Println("Error al realizar la busqueda")
		fmt.Println("Panic Call: ", err)
	}
	i := 0
	if docs.Hits.TotalHits > 0 {

		for _, item := range docs.Hits.Hits {
			var datosC correo.ObjetoCorreo
			err := json.Unmarshal(*item.Source, &datosC)
			if err != nil {
				fmt.Println("Fallo de desserealizacion")
			}
			datosC.Saldo = float64(int(datosC.Saldo*100)) / 100
			datosC.DateVencimiento, _ = time.Parse(time.RFC3339Nano, datosC.Vencimiento)
			datosC.DateEmision, _ = time.Parse(time.RFC3339Nano, datosC.Emision)
			datosC.ShortVencimiento = datosC.DateVencimiento.Format("02/01/2006")
			datosC.ShortEmision = datosC.DateEmision.Format("02/01/2006")

			if datosC.Correo != "" && datosC.Saldo > 0 && time.Now().After(datosC.DateVencimiento) {
				retorno = append(retorno, datosC)
				i++
			}
		}
	}
	if i > 0 {
		var sliceCorreo correo.SliceRegistros
		sliceCorreo = retorno
		sort.Sort(sliceCorreo)
		retorno = sliceCorreo
		consulta = `<table class="table table-stripped">`
		consulta += fmt.Sprintf(`<thead>`)
		consulta += fmt.Sprintf(`<th>Serie/Folio</th>`)
		consulta += fmt.Sprintf(`<th>Movimiento</th>`)
		consulta += fmt.Sprintf(`<th>No. Cliente</th>`)
		consulta += fmt.Sprintf(`<th>Nombre</th>`)
		consulta += fmt.Sprintf(`<th>Correo</th>`)
		consulta += fmt.Sprintf(`<th>Emision</th>`)
		consulta += fmt.Sprintf(`<th>Vencimiento</th>`)
		consulta += fmt.Sprintf(`<th>Saldo</th>`)
		consulta += fmt.Sprintf(`</thead>`)
		consulta += `<tbody>`
		for _, v := range retorno {
			consulta += fmt.Sprintf(`
			
			<tr id="%v" ondblclick="showListOverdueByClient('%v', '%v', '%v',this)">
				<td>
                	<input type="hidden" value="%v" id="Serie%v">
                	<input type="hidden" value="%v" id="Folio%v"> %v/%v
            	</td>
            	
            	<td>
                	<input type="hidden" value="%v" id="Movimiento%v"> %v
            	</td>
            	<td>
                	<input type="hidden" value="%v" id="Cliente%v"> %v
            	</td>
            	<td>
	                <input type="hidden" value="%v" id="Nombre%v">
	                <input type="hidden" value="%v" id="Codigo%v"> %v
	            </td>
            	
	            <td>
	                <input type="hidden" value="%v" id="Correo%v"> %v
	            </td>
	            <td>
	                <input type="hidden" value="%v" id="Emision%v"> %v
	            </td>

	            <td>
	                <input type="hidden" value="%v" id="Codigo%v"> %v
	            </td>
	            <td class="dineros">
	                <input type="hidden" value="%v" id="Saldo%v"> %v
	            </td>
	            <td>
	                <input type="button" value="Enviar" onclick="enviarCorreo('%v', '%v', '%v', '%v', '%v', '%v','%v',this)">
	            </td>
	        </tr>
	            
			`, v.Codigo, v.Codigo, v.Nombre, v.Correo,
				v.Serie, v.Movimiento,
				v.Folio, v.Movimiento, v.Serie, v.Folio,
				v.Movimiento, v.Movimiento, v.Movimiento,
				v.Codigo, v.Movimiento, v.Codigo,
				v.Nombre, v.Movimiento,
				v.Codigo, v.Movimiento, v.Nombre,
				v.Correo, v.Movimiento, v.Correo,
				v.Emision, v.Movimiento, v.ShortEmision,
				v.Vencimiento, v.Movimiento, v.ShortVencimiento,
				v.Saldo, v.Movimiento, v.Saldo,
				v.Nombre, v.Serie, v.Folio, v.ShortEmision, v.ShortVencimiento, v.Saldo, v.Correo)
		}
		consulta += `</tbody>`
		consulta += `</table>`
	} else {
		consulta = ""
	}

	return consulta
}

func EnviarCorreo(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		var email string
		var contenido string

		email = r.FormValue("Correo")
		contenido = r.FormValue("Contenido")
		// fmt.Println(email, contenido)
		data := correo.SendEmail(email, contenido)
		// if correo.SendEmail(email, contenido) {
		// 	data = "Correo Enviado"
		// } else {
		// 	data = "No"
		// }
		fmt.Println("Data", data)
		fmt.Println("COrreo: ", email)
		fmt.Fprintln(w, data)
	}
}
