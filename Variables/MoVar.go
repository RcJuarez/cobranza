package MoVar

import (
	"fmt"
	"reflect"
	"strings"

	config "github.com/robfig/config"
)

const (

	//############# ARCHIVOS LOCALES ######################################

	//FileConfigName contiene el nombre del archivo CFG
	FileConfigName = "Acfg.cfg"
	//DatosConfiguracion contiene los datos del archivo de configuracion
	//################ SECCIONES CFG  ######################################

	//SecDefault nombre de la seccion default del servidor en CFG
	SecDefault = "DEFAULT"
	//SecMongo nombre de la seccion de mongo en CFG
	SecMongo = "CONFIG_DB_MONGO"
	//SecPsql nombre de la seccion de postgresql en cfg
	SecPsql = "CONFIG_DB_POSTGRES"
	//SecElastic nombre de la seccion de postgresql en cfg
	SecElastic = "CONFIG_DB_ELASTIC"
	//SecCorreos nombre de la seccion de los correos en cfg
	SecCorreos = "CONFIG_DB_CORREO"
)

//DatosConfiguracion contiene la informacion del archivo de configuracion
var DatosConfiguracion InformacionCFG

//DataCfg estructura de datos del entorno
type DataCfg struct {
	BaseURL    string
	Servidor   string
	Puerto     string
	Usuario    string
	Pass       string
	Protocolo  string
	NombreBase string
}

//DataCfg estructura de datos del entorno
type DataCfgs struct {
	BaseURL    string
	Servidor   []string
	Puerto     []string
	Usuario    []string
	Pass       []string
	Protocolo  string
	NombreBase string
}

//InformacionCFG Estructura que contiene la informacion de todas las secciones del archivo de configuracion
type InformacionCFG struct {
	InfDefault, InfMongo, InfPsql, InfElastic DataCfg
	InfCorreos                                *DataCfgs
}

//#################<Funciones Generales>#######################################

//CargaSeccionCFG rellena los datos de la seccion a utilizar
func CargaSeccionCFG(fileConfig *config.Config, seccion string) (DataCfg, error) {

	var d DataCfg
	var err error
	if fileConfig.HasOption(seccion, "baseurl") {
		d.BaseURL, err = fileConfig.String(seccion, "baseurl")
		if err != nil {
			return d, err
		}
	}
	if fileConfig.HasOption(seccion, "servidor") {
		d.Servidor, err = fileConfig.String(seccion, "servidor")
		if err != nil {
			return d, err
		}
	}
	if fileConfig.HasOption(seccion, "puerto") {
		d.Puerto, err = fileConfig.String(seccion, "puerto")
		if err != nil {
			return d, err
		}
	}
	if fileConfig.HasOption(seccion, "usuario") {
		d.Usuario, err = fileConfig.String(seccion, "usuario")
		if err != nil {
		}
	}
	if fileConfig.HasOption(seccion, "pass") {
		d.Pass, err = fileConfig.String(seccion, "pass")
		if err != nil {
			return d, err
		}
	}
	if fileConfig.HasOption(seccion, "protocolo") {
		d.Protocolo, err = fileConfig.String(seccion, "protocolo")
		if err != nil {
			return d, err
		}
	}
	if fileConfig.HasOption(seccion, "base") {
		d.NombreBase, err = fileConfig.String(seccion, "base")
		if err != nil {
			return d, err
		}
	}
	return d, err
}

//CargaInformacionCFG rellena una estructura con todos los elementos del archivo de configuracion
func CargaInformacionCFG() (*InformacionCFG, error) {
	//pregunta si la variable DatosConfiguracion contiene elementos, regresa los elementos en caso de ser verdadero
	if (InformacionCFG{}) != DatosConfiguracion {
		return &DatosConfiguracion, nil
	}
	//Si la variable es vacia, entonces procede a rellenarlo (leer el archivo de configuracion con sus respectivas secciones)
	informacionCFG := InformacionCFG{}
	var FileConfig, err = config.ReadDefault(FileConfigName)
	if err == nil {
		//Carga la seccion DEFAULT
		secDefault, err := CargaSeccionCFG(FileConfig, SecDefault)
		if err == nil {
			informacionCFG.InfDefault = secDefault
		} else {
			return &DatosConfiguracion, err
		}
		//Carga la seccion MONGO
		secMongo, err := CargaSeccionCFG(FileConfig, SecMongo)
		if err == nil {
			informacionCFG.InfMongo = secMongo
		} else {
			return &DatosConfiguracion, err
		}
		//Carga la seccion POSTGRES
		secPsql, err := CargaSeccionCFG(FileConfig, SecPsql)
		if err == nil {
			informacionCFG.InfPsql = secPsql
		} else {
			return &DatosConfiguracion, err
		}
		//Carga la seccion ELASTIC
		secElastic, err := CargaSeccionCFG(FileConfig, SecElastic)
		if err == nil {
			informacionCFG.InfElastic = secElastic
		} else {
			return &DatosConfiguracion, err
		}
		//Carga la seccion CORREOS
		secCorreos, err := CargaSeccionCFG(FileConfig, SecCorreos)
		if err == nil {
			dataCfgs := &DataCfgs{}
			dataCfgs.Servidor = strings.Split(secCorreos.Servidor, "|")
			dataCfgs.Usuario = strings.Split(secCorreos.Usuario, "|")
			dataCfgs.Puerto = strings.Split(secCorreos.Puerto, "|")
			dataCfgs.Pass = strings.Split(secCorreos.Pass, "|")
			informacionCFG.InfCorreos = dataCfgs
		} else {
			return &DatosConfiguracion, err
		}
	}
	DatosConfiguracion = informacionCFG
	return &DatosConfiguracion, err
}

//CheckStruct reflects struct variables and its values and print them
func CheckStruct(object interface{}) {
	fmt.Println("#################### CHEKING OBJECT ######################")
	if reflect.ValueOf(object).Kind() == reflect.Struct {
		s := reflect.New(reflect.TypeOf(object)).Elem()
		typeOfT := s.Type()
		fmt.Println()
		fmt.Println("#### STRUCT ####")
		fmt.Println(typeOfT.String(), " ", s.Kind().String(), "{")
		for i := 0; i < s.NumField(); i++ {
			f := s.Field(i)
			fmt.Printf("   %s   %s =  %v\n", typeOfT.Field(i).Name, f.Type(), reflect.ValueOf(object).Field(i).Interface())
		}
		fmt.Println("}")
	} else {
		fmt.Println(" Object is ", reflect.ValueOf(object).Kind().String(), " Not a Struct.")
	}
	fmt.Println("#################### OBJECT CHECKED ######################")
}
