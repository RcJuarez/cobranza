package main

import (
	"fmt"
	"net/http"

	"./Controladores"
	"./Controladores/VistaVencidas"
	"./ServicioCorreo"
	"./Variables"
	"github.com/gorilla/mux"
)

//inicio

//##################################################   M   A   I   N   ############################################################
func main() {
	fmt.Println("Iniciando Servicio de Correo...")
	go servicio.Daemon()
	fmt.Println("Servicio de Correo iniciado.")
	http.Handle("/Recursos/", http.StripPrefix("/Recursos/", http.FileServer(http.Dir("Recursos"))))

	//Index
	var router = mux.NewRouter()
	http.Handle("/", router)
	router.HandleFunc("/", makeHandler(VistaVencidas.ShowList))
	//VistaVencidas.ShowList

	router.HandleFunc("/dashboard", makeHandler(controllers.IndiceGeneral))
	router.HandleFunc("/set", makeHandler(controllers.Set)).Methods("POST")
	router.HandleFunc("/config", makeHandler(controllers.SetView))

	router.HandleFunc("/vencidas", makeHandler(VistaVencidas.ShowList))
	router.HandleFunc("/vencidas/{p}", makeHandler(VistaVencidas.ShowList))
	router.HandleFunc("/vencidasCliente", makeHandler(VistaVencidas.ShowListByClient))
	router.HandleFunc("/vencidasClNoCl", makeHandler(VistaVencidas.SearchClientBills))
	router.HandleFunc("/enviarcorreo", makeHandler(VistaVencidas.EnviarCorreo))

	//###################### CFG ######################################
	DataCfg, err := MoVar.CargaInformacionCFG()
	if err != nil {
		fmt.Println("ERrores al cargar el archivo de configuracion: ", err)
	} else {
		/*
			fmt.Println("Leer los Default: ", DataCfg.InfDefault)
			fmt.Println("======================================")
			fmt.Println("Leer los InfMongo: ", DataCfg.InfMongo)
			fmt.Println("======================================")
			fmt.Println("Leer los InfPsql: ", DataCfg.InfPsql)
			fmt.Println("======================================")
			fmt.Println("Leer los InfElastic: ", DataCfg.InfElastic)
			fmt.Println("======================================")
			fmt.Println("Leer los correos: ", DataCfg.InfCorreos)
		*/

		var server http.Server
		if DataCfg.InfDefault.Puerto != "" {
			fmt.Println("Ejecutandose en el puerto: ", DataCfg.InfDefault.Puerto)
			fmt.Println("Acceder a la siguiente url: ", DataCfg.InfDefault.BaseURL)
			server = http.Server{
				Addr: ":" + DataCfg.InfDefault.Puerto,
			}
		} else {
			fmt.Println("Ejecutandose en el puerto: 8082")
			fmt.Println("Acceder a la siguiente url: localhost")
			server = http.Server{
				Addr: ":8082",
			}
		}

		server.ListenAndServe()
	}
}

//############################################### F  U  N  C  I  O  N  E S ########################################################
func makeHandler(fn func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(w, r)
	}
}
