$(document).ready(function(){

		$('#filer_input1').filer({
			showThumbs: true,
			addMore: true,
			allowDuplicates: false,
			extensions: ["pdf"],
			onSelect: function() {
    			$("#botonenviararch").prop('disabled', false);

			},
			onEmpty: function() {
    			$("#botonenviararch").prop('disabled', true);

			},
			captions : {
	    			button: "Subir Archivos",
	    			feedback: "archivos de tipo pdf.",
	    			feedback2: "Archivo(s) a subir.",
	    			drop: "Deposita aqui tu pdf",
	    			removeConfirmation: "¿Quieres remover este pdf?",
	    			errors: {
	        			filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
	        			filesType: "Solo se permiten subir archivos pdf.",
	        			filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
	        			filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
	        			folderUpload: "You are not allowed to upload folders."
	    				}
				}
		});

		$('#filer_input').filer({
			showThumbs: true,
			addMore: true,
			allowDuplicates: false,
			extensions: ["jpg", "png", "jpeg"],
			onSelect: function() {
			   	$("#botonenviarimg").prop('disabled', false);

			},
			onEmpty: function() {
			    $("#botonenviarimg").prop('disabled', true);

			},
			captions : {
	    			button: "Subir Imagenes",
	    			feedback: "archivos de tipo imagen.",
	    			feedback2: "Imagen(es) a subir.",
	    			drop: "Deposita aqui tu imagen.",
	    			removeConfirmation: "¿Quieres remover esta imagen?",
	    			errors: {
	        			filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
	        			filesType: "Solo se permiten subir imagenes.",
	        			filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
	        			filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
	        			folderUpload: "You are not allowed to upload folders."
	    				}
				}
		});

	$("body").on("click",".eliminar", function(e){
		$(this).parent('div').remove();
	});

	$('#myModal2').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');    	
		$("#modalform input[name=d_id]").val(id);

		var name = $(e.relatedTarget).data('name');
		$("#modalform input[name=d_name]").val(name);
	});

	$('#myModal3').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');    	
		$("#modalform2 input[name=d_id2]").val(id);

		var name = $(e.relatedTarget).data('name');
		$("#modalform2 input[name=d_name2]").val(name);
	});

	$('#myModal4').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');    	
		$("#modalform3 input[name=article_id_tags]").val(id);

		var tags = $(e.relatedTarget).data('tags');
		$("#modalform3 input[name=article_tags]").val(tags);
	});


	$( ".carousel-inner" ).each(function() {
		$( ".carousel-inner div:first-child" ).addClass( "active" );
	});

	$("#inputs_generados").empty();



});

function GetArticulo(id,name,cod,tags,clv){
	$("#contenedor1").html('<form role="form" id="modal_form"><div class="form-group"><label for="nombre_modal">Nombre:</label><input type="text" class="form-control etiquetas5" id="nombre_modal" name="nombre_modal" value="'+name+'"" readonly="true" /></div><div class="form-group">	<label for="clave_modal">Clave:</label><input type="text" class="form-control etiquetas5" id="clave_modal" name="clave_modal" value="'+clv+'" readonly="true" /></div><div class="form-group">	<label for="clave_modal">Codigo:</label><input type="text" class="form-control etiquetas5" id="cod_modal" name="cod_modal" value="'+cod+'" readonly="true" /></div><div class="form-group"><input type="text" id="id_modal" name="id_modal" value="'+id+'" hidden="true" /></div> <div class="form-group"><label for="id_modal">Tags:</label><input type="text" class="form-control etiquetas5" id="modal_tags" name="modal_tags" value="'+tags+'" readonly="true" /> </form>');	
}

function UpdateArticulo(){

	var claves = ""
	var id = $("#article_id_tags").val();
	var tags = $("#article_tags").val();


    	$.ajax({
			url: '/modificararticulo',
			type: 'POST',
			dataType: 'html',
			data:{id:id, tags:tags},
			success : function(data){
				if (data  == "Insertado"){
					var modal = document.getElementById('myModal4');
					modal.style.display = "none";
					Filtrado();				
				}else{
					alert("Ocurrio un error");
				}
			}
		});
}

function copiarAlPortapapeles(id_elemento) {
  var aux = document.createElement("input");
  aux.setAttribute("value", id_elemento);
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
}

function VisArchivos(id){
	alert(id);
}


function BorrarImagen(idimg,idart){
	
	$.ajax({
			url: '/eliminarimg',
			type: 'POST',
			dataType: 'html',
			data:{idart:idart, idimg:idimg},
			success : function(data){
				if (data  == "eliminado"){
					alert("Imagen Eliminada");
					Filtrado();
				}else{
					alert("Ocurrio un error");
				}
			}
		});


}


function VerImagen(Idimg,nombre,Iddoc){

							// Get the modal
							var modal = document.getElementById('myModal');

							// Get the image and insert it inside the modal - use its "alt" text as a caption

							id_img = Idimg+"_m";
							var img = document.getElementById(id_img);
							var modalImg = document.getElementById("img01");
							var captionText = document.getElementById("caption");



							img.onclick = function(){
							    modal.style.display = "block";
							    modalImg.src = this.src;
							    captionText.innerHTML = nombre;

							}

					
							// Get the <span> element that closes the modal
							var span = document.getElementsByClassName("closer_b")[0];

							// When the user clicks on <span> (x), close the modal
							span.onclick = function() {
							  modal.style.display = "none";
							}

							var trash = document.getElementsByClassName("eliminar_b")[0];

							trash.onclick = function() {
							  // modal.style.display = "none";

							  var r = confirm("¿eliminar?");
							  if (r == true) {
							  BorrarImagen(Idimg,Iddoc);
							  } else {
							      return false;
							  }

							}			  
}


function ConocerOrden(){

	var ordenamiento = ""

	menor_pr = $("#menor_pr").is(":checked");
	mayor_pr = $("#mayor_pr").is(":checked");
	menor_vta = $("#menor_vta").is(":checked");
	mayor_vta = $("#mayor_vta").is(":checked");
	menor_existe = $("#menor_existe").is(":checked");
	mayor_existe = $("#mayor_existe").is(":checked");
	nombre_orden = $("#nombre_orden").is(":checked");
	marca_orden = $("#marca_orden").is(":checked");
	elastic_orden = $("#elastic_orden").is(":checked");


	if (menor_pr == true)
		ordenamiento = $("#menor_pr").val();
	if (mayor_pr == true)
		ordenamiento = $("#mayor_pr").val();
	if (menor_vta == true)
		ordenamiento = $("#menor_vta").val();
	if (mayor_vta == true)
		ordenamiento = $("#mayor_vta").val();
	if (menor_existe == true)
		ordenamiento = $("#menor_existe").val();
	if (mayor_existe == true)
		ordenamiento = $("#mayor_existe").val();
	if (nombre_orden == true)
		ordenamiento = $("#nombre_orden").val();
	if (marca_orden == true)
		ordenamiento = $("#marca_orden").val();
	if (elastic_orden == true)
		ordenamiento = $("#elastic_orden").val();

	return ordenamiento

}

function FiltradoSolicitado(){

	var filtros = [];
	
	filtro_existencia = $("#filtroexistencia").is(":checked");

	if (filtro_existencia == true){
		filtrado = $("#filtroexistencia").val();
	 	filtros.push(filtrado)
	}

	return filtros
}

function PreciosSolicitados(){
	var precios = [];

	if ($("#rango_precio_menor").val() != ""){
		precios.push($("#rango_precio_menor").val())
	}
	if ($("#rango_precio_mayor").val() != ""){
		precios.push($("#rango_precio_mayor").val())
	}
	return	precios
}

function Filtrado(ordensillo){

	var informacion = [];
		
	//SABER ORDEN ACTUAL
	x = ConocerOrden();
	busqueda = $("#searchbox").val();


	//SABER MARCHAS CHECKED
	var marcas = []

	$(".contenedormarcasunicas div.contador_de_marca").each(function(){
		var marca = $(this).find(".check_marca").is(":checked"); 
			if (marca){
				var valor = $(this).find(".check_marca").val();
				marcas.push(valor);
			}
	});


	//SABER LOS FILTROS CHECKED
	filtros = FiltradoSolicitado();


	//SABER RANGO DE PRECIOS
	var cadena_precio = ""
	precios = PreciosSolicitados();

	if (precios.length){
		cadena_precio = 'PRECIO: (>'+precios[0]+' AND <'+precios[1]+')';
		filtros.push(cadena_precio);
	}

	cadena_marcas = '(MARCA: ';

	for (i=0; i<marcas.length;i++){
		if (i > 0){
			cadena_marcas = cadena_marcas + " OR MARCA: ";
		}
		cadena_marcas = cadena_marcas + marcas[i];
	}

	cadena_marcas = cadena_marcas + ')';

	if (marcas.length){
		filtros.push(cadena_marcas);
	}


	//CREAR INPUTS PARA EL FORMULARIO   - FILTROS -
	if (filtros.length){
		for (ii=0; ii<filtros.length; ii++){

			$('<input>').attr({
				type: 'text',
				id: 'filtro_'+ii,
			    name: 'filtro_'+ii,
			    hidden:true,
			    value:filtros[ii]
			}).appendTo('#inputs_generados');
		}

		$('<input>').attr({
				type: 'text',
				id: 'total_filtros',
			    name: 'total_filtros',
			    hidden:true,
			    value:filtros.length
			}).appendTo('#inputs_generados');		
	}

	//CREAR INPUTS PARA EL FORMULARIO   - MARCAS -
	if (marcas.length){
		for (iii=0; iii<marcas.length; iii++){

			$('<input>').attr({
				type: 'text',
				id: 'marca_'+iii,
			    name: 'marca_'+iii,
			    hidden:true,
			    value:marcas[iii]
			}).appendTo('#inputs_generados');
		}

		$('<input>').attr({
				type: 'text',
				id: 'total_marcas',
			    name: 'total_marcas',
			    hidden:true,
			    value:marcas.length
			}).appendTo('#inputs_generados');
	}

		if (ordensillo == undefined)
		{
			$("#orden").val(x);
			
		} else{
			$("#orden").val(ordensillo);
		}
		
	informacion[0]=RangosON();
	informacion[1]=ExistenciaON();
	informacion[2]=marcas;

	if (informacion[0] === false){
			alert("Algo fue mal con el rango");
			 $( "#rango_precio_menor" ).focus();
			return false;
;	}

	var cadena_info = "";

	cadena_info = "precios:"+informacion[0] +"&existencia:"+informacion[1] +"&marcas:"+informacion[2];

	console.log(cadena_info);

		$('<input>').attr({
			type: 'text',
			id: 'informacion',
		    name: 'informacion',
		    hidden:true,
		    value:cadena_info
		}).appendTo('#inputs_generados');

	 $("#searchform").trigger( "submit");
}

function ExistenciaON(){
	var array_exist = []
	filtro_existencia = $("#filtroexistencia").is(":checked");
	array_exist[0]=(filtro_existencia);
	return array_exist;
}

function RangosON(){

	var rangos = [];

	rango_precio_menor=$("#rango_precio_menor").val();
	rango_precio_mayor=$("#rango_precio_mayor").val();

	if (rango_precio_menor != "" && rango_precio_mayor != ""){
		rangos[0] = rango_precio_menor;
		rangos[1] = rango_precio_mayor;

		if (rangos[0] >= rangos[1]){
			return false;
		}
	}

	return rangos;
}