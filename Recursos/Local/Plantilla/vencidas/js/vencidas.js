$(document).ready(function () {
	$('#bloqueo').hide();

	$("#enviarLista").on('click', function () {
		correo = $("#correoTablaCliente").val();

		if (correo != "") {
			mensaje = $("#TablaVencidasCliente").html();
			console.log(mensaje);
			//estilo = "<style>table {width: 100%;   border: 1px solid #000;} th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;   border-collapse: collapse;   padding: 0.3em;   caption-side: bottom;} caption {   padding: 0.3em;   color: #fff;    background: #000;} th {    background: #eee;}</style>"
			estilo = "<style>table {  width: 100%;   border: 1px;   font-family: 'Lucida Sans Unicode', 'Lucida Grande', Sans-Serif;  font-size: 12px;   text-align: left;    border-collapse: collapse; } th {     font-size: 13px;     font-weight: normal;     padding: 8px;     background: #b9c9fe;    border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; }td {    padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;  color: #669;    border-top: 1px solid transparent; } td.dineros{text-align:rigth;}</style>";
			mensaje = estilo + mensaje;
			enviarCorreo2(mensaje, correo);
			$("#myModal").modal('hide');
			$("#tablacliente").html("")
			$("#correoTablaCliente").val("");
		}
		else {
			alert("Imposible enviar mensaje, correo no valido.");
		}
	})

	$("#filtroBusqueda").on('keyup', function (e) {
		if (e.keyCode == 13) {
			valor = $("#filtroBusqueda").val();
			if (valor != "") {
				showListClientNumClient();
			} else {
				$("#filtroBusqueda").val("");
			}
		}
	});
});


function showListOverdueByClient(codigoCliente, nombreCliente, correocliente, objeto) {
	$("#myModal").modal('show');
	$("#nombrecliente").html(nombreCliente)
	$("vencidas").html("Tabla de contenidos, datos de  facturas vencidas por cliente")
	console.log("id: ", objeto.id)
	$("#identifiacdorFilaTabla").val(objeto.id)
	$.ajax({
		url: '/vencidasCliente',
		type: 'POST',
		dataType: 'html',
		async: false,
		data: { cliente: codigoCliente },
		success: function (data) {
			if (data != "") {
				$("#tablacliente").html(data);
				$("#correoTablaCliente").val(correocliente);
			} else {
				$("#tablacliente").html("<b>Sin resultados.</b>")
			}
		}
	});
}
function showListClientNumClient() {
	valor = $("#filtroBusqueda").val();
	if (valor != "") {
		$.ajax({
			url: '/vencidasClNoCl',
			type: 'POST',
			dataType: 'html',
			data: { cliente: $("#filtroBusqueda").val() },
			success: function (data) {
				if (data != "") {
					$("#zonatabla").html(data)
				} else {
					$("#zonatabla").html("<b>Sin resultados.</b>")
				}
			}
		});
	}

}

function enviarCorreo(Nombre, Serie, Folio, ShortEmision, ShortVencimiento, Saldo, CorreoE, boton) {
	// Create our number formatter.
	$("#bloqueo").show();
	Saldo = (parseFloat(Saldo) || 0).toFixed(2);

	mensaje = "Estimado <b>" + Nombre + "</b><br><br>";
	mensaje += "Le informamos que la factura con Serie/Folio: <b>" + Serie + "</b>/<b>" + Folio + "</b><br>";
	mensaje += "Con fecha de emision: <b>" + ShortEmision + "</b> <p> y monto <b>$" + Saldo + "</b> aparece en sistema con estatus ";
	mensaje += "<b>Vencida</b>  desde la fecha:  <b>" + ShortVencimiento + "</b>.<br><br>";
	mensaje += "Si usted ya ha realizado el pago haga caso omiso de este mensaje.<br><br>";
	mensaje += "Sus amigos de Tubos y Conexiones S.A. de C.V.";
	$.ajax({
		url: '/enviarcorreo',
		type: 'POST',
		async: false,
		dataType: 'json',
		data: { Correo: CorreoE, Contenido: mensaje },
		success: function (data) {
			if (data === true) {
				console.log('------------------------------------');
				console.log("Correo Enviado de manera a exitosa.");
				console.log("data: ", data);
				console.log('------------------------------------');
				alertify.set('notifier', 'position', 'bottom-right');
				alertify.success('Correo Enviado de manera a exitosa.');
				fila = boton.parentElement.parentElement;
				fila.className = "";
				fila.className += "bg-success";
			} else {
				console.log('------------------------------------');
				console.log("Ha ocurrido un problema al enviar el correo, vuelva a intentarlo.");
				console.log("data: ", data);
				console.log('------------------------------------');
				alertify.set('notifier', 'position', 'bottom-right');
				alertify.error('Ha ocurrido un problema al enviar el correo, vuelva a intentarlo.');
				fila = boton.parentElement.parentElement;
				fila.className = "";
				fila.className += "bg-danger";
			}
			$("#bloqueo").hide();
		},
		error: function (data) {
			$("#bloqueo").hide();
		}
	});

}

function enviarCorreo2(mensaje, CorreoE) {
	$("#bloqueo").show();
	$.ajax({
		url: '/enviarcorreo',
		type: 'POST',
		async: false,
		dataType: 'json',
		data: { Correo: CorreoE, Contenido: mensaje },
		success: function (data) {
			if (data === true) {
				console.log('------------------------------------');
				console.log("Correo Enviado de manera a exitosa.");
				console.log("data: ", data);
				console.log('------------------------------------');
				alertify.set('notifier', 'position', 'bottom-right');
				alertify.success('Correo Enviado de manera a exitosa.');
				fila = document.getElementById($("#identifiacdorFilaTabla").val());
				fila.className = "";
				fila.className += "bg-success";
			} else {
				console.log('------------------------------------');
				console.log("Ha ocurrido un problema al enviar el correo, vuelva a intentarlo.");
				console.log("data: ", data);
				console.log('------------------------------------');
				alertify.set('notifier', 'position', 'bottom-right');
				alertify.error('Ha ocurrido un problema al enviar el correo, vuelva a intentarlo.');
				fila = document.getElementById($("#identifiacdorFilaTabla").val());
				fila.className = "";
				fila.className += "bg-danger";
			}
			$("#bloqueo").hide();
		},
		error: function (data) {
			$("#bloqueo").hide();
		}
	});
}

Number.prototype.format = function (n, x) {
	var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};
