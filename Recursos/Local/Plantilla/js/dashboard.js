$( document ).ready(function() {

    message('Configuracion de datos!',1);
        $("#frecuencia").on('keyup', function (e) {

                validar = $("#frecuencia").val();
                if( isNaN(validar)){
                        $("#frecuencia").val(ValorPrevio);
                }else{
                        if(validar >= 1)
                                validar = Math.floor(validar);
                        else
                                validar = 0;

                         $("#frecuencia").val(validar)
                }
        });

        $("#frecuencia").on('focus', function (e) {
                ValorPrevio = $("#frecuencia").val();
        });

        $("#frecuencia").on('blur', function (e) {
                validar = $("#frecuencia").val();
                if( isNaN(validar)){
                        $("#frecuencia").val(ValorPrevio);
                }

        });

        $("#antesde").on('keyup', function (e) {

                validar = $("#antesde").val();
                if( isNaN(validar)){
                        $("#antesde").val(ValorPrevio);
                }else{
                        if(validar >= 1)
                                validar = Math.floor(validar);
                        else
                                validar = 0;

                         $("#antesde").val(validar)
                }
        });

        $("#antesde").on('focus', function (e) {
                ValorPrevio = $("#antesde").val();
        });

        $("#antesde").on('blur', function (e) {
                validar = $("#antesde").val();
                if( isNaN(validar)){
                        $("#antesde").val(ValorPrevio);
                }

        });




});
$( document ).ajaxStart(function() {
  $( "#loading" ).show();
});
/*
* @melchor
* Funcion que maneja los mensajes de error
* @params msg:texto del mensaje,st:estado del mensaje
* @return mensaje jGrowl
 */
function message(msg,st) 
{
    if(st=='0'){
        var tema = 'jGrowl-error';
        var stk = true;
    }else if(st=='1'){
        var tema = 'jGrowl-succes';
        var stk = false;
    }
$.jGrowl(msg, {
        header: 'Mensaje:',
        theme: tema,
        sticky:stk,
        life:'2000'
    });

}
function setConfiguracion(){

	var frecuencia = $("#frecuencia").val();
	var antesde = $("#antesde").val();
	
	if (frecuencia == ""){
		alert("¡Lo campos no pueden estar vacios!");
	}else{
		if (isNaN(parseInt(frecuencia)) || isNaN(parseInt(antesde))) {
            message('¡Los campos deben ser numericos!'); 
		}else{	
			$.ajax({
				url: '/set',
				type: 'POST',
				dataType: 'html',
				data:{ frecuencia : frecuencia,antesde :antesde,
                        lunes: $("#lunes").val(), martes: $("#martes").val(), 
                        miercoles: $("#miercoles").val(), jueves: $("#jueves").val(), 
                        viernes: $("#viernes").val(), sabado: $("#sabado").val(), 
                        domingo: $("#domingo").val(), 
                },
				success : function(data){
					$('#contenedor').html(data);
				}
			});
		}
	}
}
function changeValue(x){
    $("seleccion") = x;
    if($("seleccion").val()==true)
        $("seleccion").val(false);
    else
        $("seleccion").val(true);
    console.log('------------------------------------');
    console.log($("seleccion").val());
    console.log('------------------------------------');
}

